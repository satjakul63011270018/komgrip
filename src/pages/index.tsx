import axios from "axios";
import { useEffect, useState } from "react";
import CountUp from "react-countup";
import { Button, Drawer, Radio, Space } from "antd";
import type { DrawerProps, RadioChangeEvent } from "antd";
import {
  UilStore,
  UilUser,
  UilBitcoinCircle,
  UilAngleLeft,
  UilAngleRight,
  UilBars,
} from "@iconscout/react-unicons";
interface Asset {
  id: string;
  rank: string;
  symbol: string;
  name: string;
  supply: string;
  maxSupply: string | null;
  marketCapUsd: string;
  volumeUsd24Hr: string;
  priceUsd: string;
  changePercent24Hr: string;
  vwap24Hr: string | null;
}

export default function Home() {
  const [assets, setAssets] = useState<Asset[]>([]);
  const [open, setOpen] = useState(false);

  const showDrawer = () => {
    setOpen(true);
  };

  const onClose = () => {
    setOpen(false);
  };

  const [countUilAngleRight, setCountUilAngleRight] = useState<number>(5);

  const incrementUilAngleRight = () => {
    setCountUilAngleRight(countUilAngleRight + 5);
  };

  const decrementUilAngleLeft = () => {
    setCountUilAngleRight(countUilAngleRight - 5);
  };

  useEffect(() => {
    axios
      .get("https://komgrip.co.th/coincap/assets")
      .then((response) => {
        setAssets(response.data.data);
      })
      .catch((error) => {
        console.log("error.message", error.message);
      });
  }, []);

  function formatPriceById(assetId: string): string {
    const asset = assets.find((x: Asset) => x.id === assetId)?.priceUsd;
    return asset ? parseFloat(asset).toFixed(2) : "0.00";
  }

  function getChangePercent24HrById(assetId: string): string {
    const asset = assets.find((x: Asset) => x.id === assetId);
    return asset && asset.changePercent24Hr
      ? parseFloat(asset.changePercent24Hr).toFixed(2)
      : "0";
  }

  const AssetCard = ({ assetId }: any) => {
    const asset = assets.find((x) => x.id === assetId);
    const changePercent = parseFloat(getChangePercent24HrById(assetId));

    return (
      <div className="flex h-full w-full items-center justify-between rounded-[30px] link-ac bg-white p-[15px]">
        <div>
          <h6 className="text-[14px]">{asset?.name}</h6>
          <p className="text-[18px] font-semibold">
            $
            <CountUp
              start={0}
              end={parseFloat(formatPriceById(assetId)) || 0}
              decimals={2}
              delay={0.5}
              duration={2}
              separator=","
            />
          </p>
          <p
            className={`text-[14px] ${
              changePercent < 0 ? "text-red-500" : "text-green-400"
            }`}
          >
            {changePercent}%
          </p>
        </div>
        <div className="link-ac bg-gradient-primary p-3">
          <UilBitcoinCircle className="text-white" />
        </div>
      </div>
    );
  };

  return (
    <>
      <Drawer
        title=""
        placement={"left"}
        width={250}
        onClose={onClose}
        open={open}
        closeIcon={false}
      >
        <>
          <div className="flex justify-center">
            <div className="flex-col">
              <p className="text-[#344767] flex items-center mb-5">
                <img
                  src="/img/logo.png"
                  className="w-[32px] h-[32px] me-2"
                  alt=""
                />{" "}
                Komgrip Technologies
              </p>
              <div className="w-1/2 h-1 hr-gradient mb-5"></div>
              <ul className="text-[14px] ">
                <li className="bg-white flex items-center rounded-[10px] link-ac p-3 my-3 cursor-pointer transition ease-in-out">
                  <div className="link-ac w-[32px] h-[32px] flex justify-center items-center box-ac shadow rounded-[10px] me-3">
                    <UilStore className={"w-[18px] h-[18px] text-white"} />
                  </div>
                  <span className="text-[#344767] font-semibold">
                    Dashboard
                  </span>
                </li>
                <li className="rounded-[10px] flex items-center p-3 my-3 list-menu transition ease-in-out">
                  {" "}
                  <div className="link-ac p-[15px] bg-white shadow rounded-[10px] me-3"></div>
                  Menu 1
                </li>
                <li className="rounded-[10px] flex items-center  p-3 my-3 list-menu transition ease-in-out">
                  {" "}
                  <div className="link-ac p-[15px] bg-white shadow rounded-[10px] me-3"></div>
                  Menu 2
                </li>
                <li className="rounded-[10px] flex items-center  p-3 my-3 list-menu transition ease-in-out">
                  {" "}
                  <div className="link-ac p-[15px] bg-white shadow rounded-[10px] me-3"></div>
                  Menu 3
                </li>
                <li className="rounded-[10px] flex items-center  p-3 my-3 list-menu transition ease-in-out">
                  {" "}
                  <div className="link-ac p-[15px] bg-white shadow rounded-[10px] me-3"></div>
                  Menu 3
                </li>
              </ul>
            </div>
          </div>
        </>
      </Drawer>
      <div className="flex">
        <div className="w-[350px] sm:hidden lg:hidden h-[100vh]">
          <div className="flex justify-center py-10 px-3">
            <div className="flex-col">
              <p className="text-[#344767] flex items-center mb-5">
                <img
                  src="/img/logo.png"
                  className="w-[32px] h-[32px] me-2"
                  alt=""
                />{" "}
                Komgrip Technologies
              </p>
              <div className="w-1/2 h-1 hr-gradient mb-5"></div>
              <ul className="text-[14px] ">
                <li className="bg-white flex items-center rounded-[10px] link-ac p-3 my-3 cursor-pointer transition ease-in-out">
                  <div className="link-ac w-[32px] h-[32px] flex justify-center items-center box-ac shadow rounded-[10px] me-3">
                    <UilStore className={"w-[18px] h-[18px] text-white"} />
                  </div>
                  <span className="text-[#344767] font-semibold">
                    Dashboard
                  </span>
                </li>
                <li className="rounded-[10px] flex items-center p-3 my-3 list-menu transition ease-in-out">
                  {" "}
                  <div className="link-ac p-[15px] bg-white shadow rounded-[10px] me-3"></div>
                  Menu 1
                </li>
                <li className="rounded-[10px] flex items-center  p-3 my-3 list-menu transition ease-in-out">
                  {" "}
                  <div className="link-ac p-[15px] bg-white shadow rounded-[10px] me-3"></div>
                  Menu 2
                </li>
                <li className="rounded-[10px] flex items-center  p-3 my-3 list-menu transition ease-in-out">
                  {" "}
                  <div className="link-ac p-[15px] bg-white shadow rounded-[10px] me-3"></div>
                  Menu 3
                </li>
                <li className="rounded-[10px] flex items-center  p-3 my-3 list-menu transition ease-in-out">
                  {" "}
                  <div className="link-ac p-[15px] bg-white shadow rounded-[10px] me-3"></div>
                  Menu 3
                </li>
              </ul>
            </div>
          </div>
        </div>
        <div className="w-full ps-10 pe-10 sm:p-5 pt-8">
          <div className="flex justify-between items-center">
            <div>
              <div className="flex items-center">
                <span className="text-[#344764] opacity-50 text-[14px]">
                  Pages
                </span>{" "}
                <span className="text-[#6c757d] text-[13px] ms-1 me-1">/</span>{" "}
                <span className="text-[14px]">Dashboard</span>
              </div>
              <p className="mt-1 font-semibold">Dashboard</p>
            </div>
            <div className="flex">
              <div className="flex items-center sm:hidden me-3">
                <UilUser className="w-[20px] h-[20px]" />{" "}
                <span className="ms-1 text-[14px]">Satjakul Roobngam</span>
              </div>
              <button onClick={showDrawer} className="hidden lg:flex sm:hidden">
                <UilBars />
              </button>
            </div>
          </div>
          <div className="sm:mb-6 sm:mt-6 sm:flex justify-between items-center hidden pe-2">
            <div className="flex">
              <UilUser className="w-[20px] h-[20px]" />{" "}
              <span className="ms-1 text-[14px]">Satjakul Roobngam</span>
            </div>
            <button onClick={showDrawer}>
              <UilBars />
            </button>
          </div>
          <div className="mb-[25px] mt-[50px] sm:mt-5 grid grid-cols-4 gap-4 3xl:grid-cols-4 xl:grid-cols-3 lg:grid-cols-2 md:grid-cols-2 sm:grid-cols-1">
            <AssetCard assetId="bitcoin" />
            <AssetCard assetId="ethereum" />
            <AssetCard assetId="solana" />
            <AssetCard assetId="dogecoin" />
          </div>
          <div className="rounded-[30px] link-ac  bg-white p-[30px]">
            <div className="flex justify-between items-center">
              <h3 className="font-semibold">Cryptocurrencies</h3>
              <div className="flex">
                <button
                  onClick={decrementUilAngleLeft}
                  className={`border-[#cb0c9f] border sm:px-3 px-7 rounded-[8px] py-1 flex justify-center items-center me-3 text-[#cb0c9f] transform active:scale-90 transition-transform duration-200 ${
                    countUilAngleRight - 5 === 0 ? "hidden" : ""
                  }`}
                >
                  <UilAngleLeft />
                </button>
                <button
                  onClick={incrementUilAngleRight}
                  className={`border-[#cb0c9f] border sm:px-3 px-7 rounded-[8px] py-1 flex justify-center items-center text-[#cb0c9f] transform active:scale-90 transition-transform duration-200 ${
                    countUilAngleRight === 100 ? "hidden" : ""
                  }`}
                >
                  <UilAngleRight />
                </button>
              </div>
            </div>
            <div className="overflow-x-auto relative sm:rounded-lg mt-5">
              <table className="w-full text-sm text-left text-gray-500">
                <thead className="text-xs text-gray-400 uppercase bg-white border-b">
                  <tr>
                    <th scope="col" className="py-3 px-6">
                      NO
                    </th>
                    <th scope="col" className="py-3 px-6">
                      NAME
                    </th>
                    <th scope="col" className="py-3 px-6 text-center">
                      SYMBOL
                    </th>
                    <th scope="col" className="py-3 px-6 text-end">
                      SUPPLY/MAX SUPPLY
                    </th>
                    <th scope="col" className="py-3 px-6 text-end">
                      USD
                    </th>
                    <th scope="col" className="py-3 px-6 text-end">
                      24 HR
                    </th>
                  </tr>
                </thead>
                <tbody>
                  {assets
                    .slice(countUilAngleRight - 5, countUilAngleRight)
                    .map((asset, index) => {
                      // Format the supply and max supply
                      const formattedSupply = asset.supply
                        ? new Intl.NumberFormat("en-US", {
                            maximumFractionDigits: 0,
                          }).format(Number(asset.supply))
                        : "0";

                      const formattedMaxSupply = asset.maxSupply
                        ? new Intl.NumberFormat("en-US", {
                            maximumFractionDigits: 0,
                          }).format(Number(asset.maxSupply))
                        : "NO LIMIT";

                      return (
                        <tr key={asset.id} className="bg-white border-b">
                          <td className="py-4 px-6">{asset.rank}</td>
                          <td className="py-4 px-6">{asset.name}</td>
                          <td className="py-4 px-6 text-center">
                            {asset.symbol}
                          </td>
                          <td className="py-4 px-6 text-end">
                            {formattedSupply} / {formattedMaxSupply}
                          </td>
                          <td className="py-4 px-6 text-end">
                            ${parseFloat(asset.priceUsd).toFixed(2)}
                          </td>
                          <td
                            className={`py-4 px-6 text-end ${
                              asset.changePercent24Hr.startsWith("-")
                                ? "text-red-500"
                                : "text-green-500"
                            }`}
                          >
                            {asset.changePercent24Hr.startsWith("-") ? "" : "+"}
                            {parseFloat(asset.changePercent24Hr).toFixed(2)}%
                          </td>
                        </tr>
                      );
                    })}
                </tbody>
              </table>
            </div>
          </div>

          <p className="text-[14px] sm:text-[12px] mt-10 ms-5">
            Copyright © 2022 Komgrip Technologies Co., Ltd.
          </p>
        </div>
      </div>
    </>
  );
}
