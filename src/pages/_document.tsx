import { Html, Head, Main, NextScript } from "next/document";

export default function Document() {
  return (
    <Html lang="en">
      <Head>
        <link rel="icon" href="/img/logo.png" type="image/x-icon" />
      </Head>
      <body className="text-[#344767]">
        <title>Komgrip - Test</title>
        <Main />
        <NextScript />
      </body>
    </Html>
  );
}
