import type { Config } from "tailwindcss";

const config: Config = {
  content: ["./index.html", "./src/**/*.{js,ts,jsx,tsx}"],
  theme: {
    extend: {},
    screens: {
      "4xl": { max: "1699px" },
      "3xl": { max: "1599px" },
      "2xl": { max: "1299px" },
      xl: { max: "1199px" },
      lg: { max: "991px" },
      md: { max: "767px" },
      sm: { max: "575px" },
      ssm: { max: "480px" },
      xs: { max: "380px" },
      xxs: { max: "320px" },
      "min-xxs": "320px",
      "min-xs": "380px",
      "min-ssm": "480px",
      "min-sm": "575px",
      "min-md": "768px",
      "min-lg": "991px",
      "min-xl": "1199px",
      "min-2xl": "1299px",
      "min-3xl": "1599px",
      "min-4xl": "1699px",
    },
  },
  plugins: [],
};
export default config;
